package com.example.hp.asyntask.recycleviewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.hp.asyntask.R;
import com.example.hp.asyntask.models.MainResponse;
import com.example.hp.asyntask.models.Response;

import java.util.ArrayList;

/**
 * Created by HP on 22/03/2018.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
    private final Context context;
    private ArrayList<Response> albumList;

    public AlbumAdapter(ArrayList<Response> album, Context context) {
        //this.albumList = album.responses;
        this.albumList = album;
        this.context = context;
    }

    @Override
    public AlbumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_list, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(AlbumAdapter.ViewHolder holder, int position) {
        Response response = albumList.get(position);
        holder.title.setText(response.getTitle());
        holder.Id.setText(response.getId() + "");
        // holder.image.setImage(response.getThumbnailUrl());
        Glide.with(context).load(response.getUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, Id;
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tv_title);
            Id = itemView.findViewById(R.id.tv_Id);
            image = itemView.findViewById(R.id.image);

        }
    }
}
