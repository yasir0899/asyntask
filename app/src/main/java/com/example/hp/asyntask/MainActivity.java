package com.example.hp.asyntask;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hp.asyntask.models.MainResponse;
import com.example.hp.asyntask.models.Response;
import com.example.hp.asyntask.recycleviewAdapter.AlbumAdapter;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnList;
    String strUrl = "https://jsonplaceholder.typicode.com/photos";
    private String TAG = "YASIR";
    private RecyclerView rcv;
    Response response = null;
    private MainResponse mainResponse;
    private ProgressBar loading;
    ArrayList<Response> arrayList=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rcv = findViewById(R.id.rcv_album);
        rcv.setLayoutManager(new LinearLayoutManager(this));
        btnList = findViewById(R.id.btn_list);
        btnList.setOnClickListener(this);
        response = new Response();
        mainResponse=new MainResponse();
        loading = findViewById(R.id.pg_loading);
        // new MultiplyTask().execute(strUrl);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_list:

                //int i = Integer.parseInt(num1.getText().toString());
                // int j = Integer.parseInt(num2.getText().toString());
                //  strUrl = "https://jsonplaceholder.typicode.com/photos";
                Log.e(TAG, "BUTTON CLICKED");
                new MultiplyTask().execute(strUrl); //send request to the server

        }
    }

    public class MultiplyTask extends AsyncTask<String, String, MainResponse> {
        //      ProgressDialog progDailog = new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            Log.e(TAG, "onPreExecute");
            super.onPreExecute();
//            progDailog.setMessage("Loading... Plz Wiat");
//            progDailog.setIndeterminate(false);
//            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progDailog.setCancelable(true);
//            progDailog.show();
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(MainResponse s) {
            Log.e(TAG, "onPostExecute");
            super.onPostExecute(s);
            //if ( progDailog.isShowing()){   progDailog.dismiss();}
            // progDailog.dismiss();
            loading.setVisibility(View.INVISIBLE);
            if (s != null) {
                setRecycleview(s);
            } else {
                Toast.makeText(MainActivity.this, "No JSON Found", Toast.LENGTH_SHORT).show();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected MainResponse doInBackground(String... strings) {
            Log.e(TAG, "doInBackground");
            String inputLine;
            String result;
            try {
                URL url = new URL(strUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();// once you have connected you have to send request to server


                //to get responce from the server we use bufferreader
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                //String value = bufferedReader.readLine();//fetch respionce from the server
                //TODO; parse value json by Gson or LoganSquare to model
                // Log.e("value =", value);

                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while ((inputLine = bufferedReader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                bufferedReader.close();

                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
                Log.e(TAG, result);
               // Response[] albumsList = new Gson().fromJson(result, Response[].class);
                result = "{ \"albums\":" + result + "}";
                mainResponse = new Gson().fromJson(result,MainResponse.class);
               // mainResponse = albumsList;
//                ArrayList<Response> al1 = new ArrayList();
//                al1 = albumsList;
              //  arrayList = Stream.of(albumsList).collect(Collectors.toCollection(ArrayList::new));
            } catch (Exception e) {
                // e.printStackTrace();
                Log.e("Exception", e.getMessage());
            }
          // return arrayList;
          return mainResponse;
        }
    }

    private void setRecycleview(MainResponse response) {

         AlbumAdapter albumAdapter = new AlbumAdapter(response.responses, this);
       // AlbumAdapter albumAdapter = new AlbumAdapter(response, this);
        rcv.setAdapter(albumAdapter);


    }
}