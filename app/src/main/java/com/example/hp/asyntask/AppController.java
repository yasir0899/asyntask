package com.example.hp.asyntask;

import android.support.multidex.MultiDexApplication;

/**
 * Created by Yasir on 3/19/2018.
 */

public class AppController extends MultiDexApplication {
private   static AppController appContext=null;
    @Override
    public void onCreate() {
        super.onCreate();
        appContext=this;
    }
    public  static AppController getAppContext(){
        return appContext;

    }
}
